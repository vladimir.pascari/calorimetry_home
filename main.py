from functions import m_json
from functions import m_pck

path = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"
metadata = m_json.get_metadata_from_setup(path)
m_pck.time.sleep(1)
m_json.add_temperature_sensor_serials("/home/pi/calorimetry_home/datasheets", metadata)
m_pck.time.sleep(1)
measdata = m_pck.get_meas_data_calorimetry(metadata)

m_pck.logging_calorimetry(measdata, metadata, '/home/pi/calorimetry_home/Newton', '/home/pi/calorimetry_home/datasheets')

m_pck.time.sleep(1)

m_json.archiv_json("/home/pi/calorimetry_home/datasheets", "/home/pi/calorimetry_home/datasheets/setup_newton.json", "/home/pi/calorimetry_home/Newton")
